Rails.application.routes.draw do
  namespace :v1 do
    namespace :user do
      resource :contacts, only: %i[show update]
    end
    resources :cities, only: :index do
      resources :districts, only: :index, controller: 'cities/districts'
    end
  end
  mount_devise_token_auth_for 'User', at: '/v1/user'
end

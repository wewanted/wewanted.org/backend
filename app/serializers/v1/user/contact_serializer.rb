module V1
    module User
      class ContactSerializer < ::ActiveModel::Serializer
        attributes :phone

        belongs_to :city,     serializer: ::V1::City::IndexSerializer
        belongs_to :district, serializer: ::V1::City::District::IndexSerializer
      end
    end
  end
  
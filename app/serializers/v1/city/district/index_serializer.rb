module V1
  module City
    module District
      class IndexSerializer < ::ActiveModel::Serializer
        attributes :id, :name
      end
    end
  end
end

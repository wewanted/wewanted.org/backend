module V1
  module City
    class IndexSerializer < ::ActiveModel::Serializer
      attributes :id, :name 
    end
  end
end

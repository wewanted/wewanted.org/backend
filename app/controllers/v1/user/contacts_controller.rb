module V1
    module User
        class ContactsController < ApplicationController
            include V1::User::ContactsHelper

            before_action :authenticate_user!
            before_action :user!

            def show
                render json: @contact, serializer: ::V1::User::ContactSerializer
            end

            def update
                @contact.update(user_contact_parameters) ? update_success : update_fail
            end

            private 
            
            def user!
                @user    = current_user
                @contact = @user.contact
            end

            def user_contact_parameters
                params.permit(:phone, :city_id, :district_id)
            end
        end
    end
end

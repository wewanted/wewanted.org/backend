module V1
  module Cities
    class DistrictsController < ApplicationController
      before_action :city!

      def index
        @districts = @city.districts

        render json: @districts, each_serializer: ::V1::City::District::IndexSerializer
      end

      private

      def city!
        city_id = params.fetch(:city_id, nil)
        @city   = ::City.find(city_id)
      end
    end
  end
end
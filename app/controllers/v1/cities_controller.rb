module V1
  class CitiesController < ::ApplicationController
    def index
      @cities = ::City.all

      render json: @cities, each_serializer: V1::City::IndexSerializer
    end
  end
end

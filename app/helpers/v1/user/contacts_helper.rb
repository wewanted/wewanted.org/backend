module V1
    module User
        module ContactsHelper
            I18N_PREFIX = 'controllers.v1.user.contacts_controller'
            def update_success
                message = [I18N_PREFIX, 'update', 'success'].join('.')
                
                render json: {
                    status: :success,
                    message: I18n.t(message)
                }, status: :ok
            end

            def update_fail
                message = [I18N_PREFIX, 'update', 'error'].join('.')

                render json: {
                    status: :error,
                    message: I18n.t(message),
                    errors: @contact.errors.messages
                }, status: :unprocessable_entity
            end
        end
    end
end
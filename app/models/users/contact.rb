# == Schema Information
#
# Table name: users_contacts
#
#  id          :bigint(8)        not null, primary key
#  phone       :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  city_id     :bigint(8)
#  district_id :bigint(8)
#  user_id     :bigint(8)
#
# Indexes
#
#  index_users_contacts_on_city_id      (city_id)
#  index_users_contacts_on_district_id  (district_id)
#  index_users_contacts_on_user_id      (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (city_id => cities.id)
#  fk_rails_...  (district_id => districts.id)
#  fk_rails_...  (user_id => users.id)
#

class Users::Contact < ApplicationRecord
  belongs_to :user
  belongs_to :city,     optional: true
  belongs_to :district, optional: true

  validates :phone, format: { 
    with: /\A[\+\d\(\)\s\-]+\z/, 
    message: :phone_character,
    allow_blank: true
  }
  validate :check_city_and_district

  before_validation :city!

  private

  def check_city_and_district
    return errors.add(:city, :city_must_be_select_when_district_selected) if city.nil? && district.present?

    return if district.nil?
    return if district.city == city

    errors.add(:district, :district_and_city_not_match, { district: district.name, city: city.name, district_city: district.city.name })
  end

  def city!
    return if city.present?
    return if district.nil?

    self.city = district.city
  end
end

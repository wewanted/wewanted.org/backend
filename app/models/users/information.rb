# == Schema Information
#
# Table name: users_information
#
#  id          :bigint(8)        not null, primary key
#  name        :string
#  picture     :string
#  surname     :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  city_id     :bigint(8)
#  district_id :bigint(8)
#  user_id     :bigint(8)
#
# Indexes
#
#  index_users_information_on_city_id      (city_id)
#  index_users_information_on_district_id  (district_id)
#  index_users_information_on_user_id      (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (city_id => cities.id)
#  fk_rails_...  (district_id => districts.id)
#  fk_rails_...  (user_id => users.id)
#

class Users::Information < ApplicationRecord
  mount_uploader :picture, ::User::PictureUploader

  belongs_to :user
  belongs_to :city,     optional: true
  belongs_to :district, optional: true

  validates :name,    presence: true
  validates :surname, presence: true

  before_save :format!

  private

  def format!
    titleize!
    escape!
  end

  def titleize!
    self.name.titleize!
    self.surname.titleize!
  end

  def escape!
    self.name = CGI.escapeHTML(name)
    self.surname = CGI.escapeHTML(surname)
  end
end

# == Schema Information
#
# Table name: districts
#
#  id         :bigint(8)        not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  city_id    :bigint(8)
#
# Indexes
#
#  index_districts_on_city_id  (city_id)
#
# Foreign Keys
#
#  fk_rails_...  (city_id => cities.id)
#
using TurkishSupport

class District < ApplicationRecord
  belongs_to :city

  has_many :user_contacts,    dependent: :nullify, class_name: 'Users::Contact'
  has_many :user_information, dependent: :nullify, class_name: 'Users::Information'

  validates :name, presence: true

  before_save :format!

  private

  def format!
    titleize!
    escape!
  end

  def titleize!
    self.name.titleize!
  end

  def escape!
    self.name = CGI.escapeHTML(name)
  end
end

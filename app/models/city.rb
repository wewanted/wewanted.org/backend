# == Schema Information
#
# Table name: cities
#
#  id         :bigint(8)        not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
using TurkishSupport

class City < ApplicationRecord
  validates :name, presence: true

  has_many :districts,        dependent: :destroy
  has_many :user_contacts,    dependent: :nullify, class_name: 'Users::Contact'
  has_many :user_information, dependent: :nullify, class_name: 'Users::Information'

  before_save :format!

  private

  def format!
    titleize!
    escape!
  end

  def titleize!
    self.name.titleize!
  end

  def escape!
    self.name = CGI.escapeHTML(name)
  end
end

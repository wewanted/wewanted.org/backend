class CreateUsersContacts < ActiveRecord::Migration[5.2]
  def change
    create_table :users_contacts do |t|
      t.references :user, foreign_key: true
      t.references :city, foreign_key: true
      t.references :district, foreign_key: true
      t.string :phone

      t.timestamps
    end
  end
end

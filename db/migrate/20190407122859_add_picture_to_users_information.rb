class AddPictureToUsersInformations < ActiveRecord::Migration[5.2]
  def change
    add_column :users_information, :picture, :string
  end
end

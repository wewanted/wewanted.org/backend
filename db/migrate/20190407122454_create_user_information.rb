class CreateUserInformation < ActiveRecord::Migration[5.2]
  def change
    create_table :user_information do |t|
      t.references :user, foreign_key: true
      t.string :name
      t.string :surname
      t.references :city, foreign_key: true
      t.references :district, foreign_key: true

      t.timestamps
    end
  end
end
